import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({navigation})=> {
    const tasks=[{
        id: 1, 
        title:'Ir ao mercado',
        date:'2024-02-27',
        time:'10:00',
        address:'Super SS'
    }, 
    {   id: 2, 
        title:'Fazer exercícios', 
        date:'2024-02-27', 
        time:'14:00', 
        address:'Academia XYZ'
    }, 

    {   id: 3, 
        title:'Ligar para o cliente', 
        date:'2024-02-28', 
        time:'15:30', 
        address:'Online'
    },
    ]
    const taskPress=(task)=>{
        navigation.navigate('DetalhesDasTarefas',{task})
    }
return(
    <View>
        <FlatList
        data={tasks}
        keyExtractor={(item)=> item.id.toString}
        renderItem={({item})=>(
            <TouchableOpacity onPress={() => taskPress (item)}>
                <Text>{item.title}</Text>
            </TouchableOpacity>
        )}
        />
    </View>
)
}
export default TaskList;